﻿// ***********************************************************************
// Assembly         : _unity
// Author           : Magnus Wolffelt, Abhishek Deb
// Created          : 04-27-2015
//
// Last Modified By : Abhishek Deb
// Last Modified On : 04-27-2015
// ***********************************************************************
// <copyright file="Messenger.cs" company="Skipbits">
//     Copyright ©  2015
// </copyright>
// <summary>
// Messenger.cs v1.0 by Magnus Wolffelt, magnus.wolffelt@gmail.com
// Version 1.4 by Julie Iaccarino, biscuitWizard @ github.com
//
// Inspired by and based on Rod Hyde's Messenger:
// http://www.unifycommunity.com/wiki/index.php?title=CSharpMessenger
//
// This is a C# messenger (notification center). It uses delegates
// and generics to provide type-checked messaging between event producers and
// event consumers, without the need for producers or consumers to be aware of
// each other. The major improvement from Hyde's implementation is that
// there is more extensive error detection, preventing silent bugs.
//
// Usage example:
// Messenger<float>.AddListener("myEvent", MyEventHandler);
// ...
// Messenger<float>.Broadcast("myEvent", 1.0f);
//
// Callback example:
// Messenger<float>.AddListener<string>("myEvent", MyEventHandler);
// private string MyEventHandler(float f1) { return "Test " + f1; }
// ...
// Messenger<float>.Broadcast<string>("myEvent", 1.0f, MyEventCallback);
// private void MyEventCallback(string s1) { Debug.Log(s1"); }

//</summary>
// ***********************************************************************
using System;
using System.Linq;
using System.Collections.Generic;

/// <summary>
/// The Events namespace.
/// </summary>
namespace _unity.Events
{

    /// <summary>
    /// Enum MessengerMode
    /// </summary>
    public enum MessengerMode
    {
        DONT_REQUIRE_LISTENER,
        REQUIRE_LISTENER,
    }

    /// <summary>
    /// Class MessengerInternal.
    /// </summary>
    static internal class MessengerInternal
    {
        
        /// <summary>
        /// The event table
        /// </summary>
        readonly public static Dictionary<string, Delegate> eventTable = new Dictionary<string, Delegate>();
        /// <summary>
        /// The defaul t_ mode
        /// </summary>
        static public readonly MessengerMode DEFAULT_MODE = MessengerMode.REQUIRE_LISTENER;

        /// <summary>
        /// Adds the listener.
        /// </summary>
        /// <param name="eventType">Type of the event.</param>
        /// <param name="callback">The callback.</param>
        static public void AddListener(string eventType, Delegate callback)
        {
            MessengerInternal.OnListenerAdding(eventType, callback);
            eventTable[eventType] = Delegate.Combine(eventTable[eventType], callback);
        }

        /// <summary>
        /// Removes the listener.
        /// </summary>
        /// <param name="eventType">Type of the event.</param>
        /// <param name="handler">The handler.</param>
        static public void RemoveListener(string eventType, Delegate handler)
        {
            MessengerInternal.OnListenerRemoving(eventType, handler);
            eventTable[eventType] = Delegate.Remove(eventTable[eventType], handler);
            MessengerInternal.OnListenerRemoved(eventType);
        }

        /// <summary>
        /// Gets the invocation list.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="eventType">Type of the event.</param>
        /// <returns>T[].</returns>
        static public T[] GetInvocationList<T>(string eventType)
        {
            Delegate d;
            if (eventTable.TryGetValue(eventType, out d))
            {
                if (d != null)
                {
                    return d.GetInvocationList().Cast<T>().ToArray();
                }
                else
                {
                    throw MessengerInternal.CreateBroadcastSignatureException(eventType);
                }
            }
            return null;
        }

        /// <summary>
        /// Called when [listener adding].
        /// </summary>
        /// <param name="eventType">Type of the event.</param>
        /// <param name="listenerBeingAdded">The listener being added.</param>
        /// <exception cref="_unity.MessengerInternal.ListenerException"></exception>
        static public void OnListenerAdding(string eventType, Delegate listenerBeingAdded)
        {
            if (!eventTable.ContainsKey(eventType))
            {
                eventTable.Add(eventType, null);
            }

            var d = eventTable[eventType];
            if (d != null && d.GetType() != listenerBeingAdded.GetType())
            {
                throw new ListenerException(string.Format("Attempting to add listener with inconsistent signature for event type {0}. Current listeners have type {1} and listener being added has type {2}", eventType, d.GetType().Name, listenerBeingAdded.GetType().Name));
            }
        }

        /// <summary>
        /// Called when [listener removing].
        /// </summary>
        /// <param name="eventType">Type of the event.</param>
        /// <param name="listenerBeingRemoved">The listener being removed.</param>
        /// <exception cref="_unity.MessengerInternal.ListenerException">
        /// </exception>
        static public void OnListenerRemoving(string eventType, Delegate listenerBeingRemoved)
        {
            if (eventTable.ContainsKey(eventType))
            {
                var d = eventTable[eventType];

                if (d == null)
                {
                    throw new ListenerException(string.Format("Attempting to remove listener with for event type {0} but current listener is null.", eventType));
                }
                else if (d.GetType() != listenerBeingRemoved.GetType())
                {
                    throw new ListenerException(string.Format("Attempting to remove listener with inconsistent signature for event type {0}. Current listeners have type {1} and listener being removed has type {2}", eventType, d.GetType().Name, listenerBeingRemoved.GetType().Name));
                }
            }
            else
            {
                throw new ListenerException(string.Format("Attempting to remove listener for type {0} but Messenger doesn't know about this event type.", eventType));
            }
        }

        /// <summary>
        /// Called when [listener removed].
        /// </summary>
        /// <param name="eventType">Type of the event.</param>
        static public void OnListenerRemoved(string eventType)
        {
            if (eventTable[eventType] == null)
            {
                eventTable.Remove(eventType);
            }
        }

        /// <summary>
        /// Called when [broadcasting].
        /// </summary>
        /// <param name="eventType">Type of the event.</param>
        /// <param name="mode">The mode.</param>
        /// <exception cref="_unity.MessengerInternal.BroadcastException"></exception>
        static public void OnBroadcasting(string eventType, MessengerMode mode)
        {
            if (mode == MessengerMode.REQUIRE_LISTENER && !eventTable.ContainsKey(eventType))
            {
                throw new MessengerInternal.BroadcastException(string.Format("Broadcasting message {0} but no listener found.", eventType));
            }
        }

        /// <summary>
        /// Creates the broadcast signature exception.
        /// </summary>
        /// <param name="eventType">Type of the event.</param>
        /// <returns>BroadcastException.</returns>
        static public BroadcastException CreateBroadcastSignatureException(string eventType)
        {
            return new BroadcastException(string.Format("Broadcasting message {0} but listeners have a different signature than the broadcaster.", eventType));
        }

        /// <summary>
        /// Class BroadcastException.
        /// </summary>
        public class BroadcastException : Exception
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="BroadcastException"/> class.
            /// </summary>
            /// <param name="msg">The MSG.</param>
            public BroadcastException(string msg)
                : base(msg)
            {
            }
        }

        /// <summary>
        /// Class ListenerException.
        /// </summary>
        public class ListenerException : Exception
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="ListenerException"/> class.
            /// </summary>
            /// <param name="msg">The MSG.</param>
            public ListenerException(string msg)
                : base(msg)
            {
            }
        }
    }

    // No parameters    
    /// <summary>
    /// Class Messenger with no parameter.
    /// </summary>
    static public class Messenger
    {
        /// <summary>
        /// Adds the listener.
        /// </summary>
        /// <param name="eventType">Type of the event.</param>
        /// <param name="handler">The handler.</param>
        static public void AddListener(string eventType, Action handler)
        {
            MessengerInternal.AddListener(eventType, handler);
        }

        /// <summary>
        /// Adds the listener.
        /// </summary>
        /// <typeparam name="TReturn">The type of the t return.</typeparam>
        /// <param name="eventType">Type of the event.</param>
        /// <param name="handler">The handler.</param>
        static public void AddListener<TReturn>(string eventType, Func<TReturn> handler)
        {
            MessengerInternal.AddListener(eventType, handler);
        }

        /// <summary>
        /// Removes the listener.
        /// </summary>
        /// <param name="eventType">Type of the event.</param>
        /// <param name="handler">The handler.</param>
        static public void RemoveListener(string eventType, Action handler)
        {
            MessengerInternal.RemoveListener(eventType, handler);
        }

        /// <summary>
        /// Removes the listener.
        /// </summary>
        /// <typeparam name="TReturn">The type of the t return.</typeparam>
        /// <param name="eventType">Type of the event.</param>
        /// <param name="handler">The handler.</param>
        static public void RemoveListener<TReturn>(string eventType, Func<TReturn> handler)
        {
            MessengerInternal.RemoveListener(eventType, handler);
        }

        /// <summary>
        /// Broadcasts the specified event type.
        /// </summary>
        /// <param name="eventType">Type of the event.</param>
        static public void Broadcast(string eventType)
        {
            Broadcast(eventType, MessengerInternal.DEFAULT_MODE);
        }

        /// <summary>
        /// Broadcasts the specified event type.
        /// </summary>
        /// <typeparam name="TReturn">The type of the t return.</typeparam>
        /// <param name="eventType">Type of the event.</param>
        /// <param name="returnCall">The return call.</param>
        static public void Broadcast<TReturn>(string eventType, Action<TReturn> returnCall)
        {
            Broadcast(eventType, returnCall, MessengerInternal.DEFAULT_MODE);
        }

        /// <summary>
        /// Broadcasts the specified event type.
        /// </summary>
        /// <param name="eventType">Type of the event.</param>
        /// <param name="mode">The mode.</param>
        static public void Broadcast(string eventType, MessengerMode mode)
        {
            MessengerInternal.OnBroadcasting(eventType, mode);
            var invocationList = MessengerInternal.GetInvocationList<Action>(eventType);

            foreach (var callback in invocationList)
                callback.Invoke();
        }

        /// <summary>
        /// Broadcasts the specified event type.
        /// </summary>
        /// <typeparam name="TReturn">The type of the t return.</typeparam>
        /// <param name="eventType">Type of the event.</param>
        /// <param name="returnCall">The return call.</param>
        /// <param name="mode">The mode.</param>
        static public void Broadcast<TReturn>(string eventType, Action<TReturn> returnCall, MessengerMode mode)
        {
            MessengerInternal.OnBroadcasting(eventType, mode);
            var invocationList = MessengerInternal.GetInvocationList<Func<TReturn>>(eventType);

            foreach (var result in invocationList.Select(del => del.Invoke()).Cast<TReturn>())
            {
                returnCall.Invoke(result);
            }
        }
    }

    // One parameter
    /// <summary>
    /// Class Messenger with 1 parameter.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    static public class Messenger<T>
    {
        /// <summary>
        /// Adds the listener.
        /// </summary>
        /// <param name="eventType">Type of the event.</param>
        /// <param name="handler">The handler.</param>
        static public void AddListener(string eventType, Action<T> handler)
        {
            MessengerInternal.AddListener(eventType, handler);
        }

        /// <summary>
        /// Adds the listener.
        /// </summary>
        /// <typeparam name="TReturn">The type of the t return.</typeparam>
        /// <param name="eventType">Type of the event.</param>
        /// <param name="handler">The handler.</param>
        static public void AddListener<TReturn>(string eventType, Func<T, TReturn> handler)
        {
            MessengerInternal.AddListener(eventType, handler);
        }

        /// <summary>
        /// Removes the listener.
        /// </summary>
        /// <param name="eventType">Type of the event.</param>
        /// <param name="handler">The handler.</param>
        static public void RemoveListener(string eventType, Action<T> handler)
        {
            MessengerInternal.RemoveListener(eventType, handler);
        }

        /// <summary>
        /// Removes the listener.
        /// </summary>
        /// <typeparam name="TReturn">The type of the t return.</typeparam>
        /// <param name="eventType">Type of the event.</param>
        /// <param name="handler">The handler.</param>
        static public void RemoveListener<TReturn>(string eventType, Func<T, TReturn> handler)
        {
            MessengerInternal.RemoveListener(eventType, handler);
        }

        /// <summary>
        /// Broadcasts the specified event type.
        /// </summary>
        /// <param name="eventType">Type of the event.</param>
        /// <param name="arg1">The arg1.</param>
        static public void Broadcast(string eventType, T arg1)
        {
            Broadcast(eventType, arg1, MessengerInternal.DEFAULT_MODE);
        }

        /// <summary>
        /// Broadcasts the specified event type.
        /// </summary>
        /// <typeparam name="TReturn">The type of the t return.</typeparam>
        /// <param name="eventType">Type of the event.</param>
        /// <param name="arg1">The arg1.</param>
        /// <param name="returnCall">The return call.</param>
        static public void Broadcast<TReturn>(string eventType, T arg1, Action<TReturn> returnCall)
        {
            Broadcast(eventType, arg1, returnCall, MessengerInternal.DEFAULT_MODE);
        }

        /// <summary>
        /// Broadcasts the specified event type.
        /// </summary>
        /// <param name="eventType">Type of the event.</param>
        /// <param name="arg1">The arg1.</param>
        /// <param name="mode">The mode.</param>
        static public void Broadcast(string eventType, T arg1, MessengerMode mode)
        {
            MessengerInternal.OnBroadcasting(eventType, mode);
            var invocationList = MessengerInternal.GetInvocationList<Action<T>>(eventType);

            foreach (var callback in invocationList)
                callback.Invoke(arg1);
        }

        /// <summary>
        /// Broadcasts the specified event type.
        /// </summary>
        /// <typeparam name="TReturn">The type of the t return.</typeparam>
        /// <param name="eventType">Type of the event.</param>
        /// <param name="arg1">The arg1.</param>
        /// <param name="returnCall">The return call.</param>
        /// <param name="mode">The mode.</param>
        static public void Broadcast<TReturn>(string eventType, T arg1, Action<TReturn> returnCall, MessengerMode mode)
        {
            MessengerInternal.OnBroadcasting(eventType, mode);
            var invocationList = MessengerInternal.GetInvocationList<Func<T, TReturn>>(eventType);

            foreach (var result in invocationList.Select(del => del.Invoke(arg1)).Cast<TReturn>())
            {
                returnCall.Invoke(result);
            }
        }
    }


    // Two parameters
    /// <summary>
    /// Class Messenger with 2 parameters.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="U"></typeparam>
    static public class Messenger<T, U>
    {
        /// <summary>
        /// Adds the listener.
        /// </summary>
        /// <param name="eventType">Type of the event.</param>
        /// <param name="handler">The handler.</param>
        static public void AddListener(string eventType, Action<T, U> handler)
        {
            MessengerInternal.AddListener(eventType, handler);
        }

        /// <summary>
        /// Adds the listener.
        /// </summary>
        /// <typeparam name="TReturn">The type of the t return.</typeparam>
        /// <param name="eventType">Type of the event.</param>
        /// <param name="handler">The handler.</param>
        static public void AddListener<TReturn>(string eventType, Func<T, U, TReturn> handler)
        {
            MessengerInternal.AddListener(eventType, handler);
        }

        /// <summary>
        /// Removes the listener.
        /// </summary>
        /// <param name="eventType">Type of the event.</param>
        /// <param name="handler">The handler.</param>
        static public void RemoveListener(string eventType, Action<T, U> handler)
        {
            MessengerInternal.RemoveListener(eventType, handler);
        }

        /// <summary>
        /// Removes the listener.
        /// </summary>
        /// <typeparam name="TReturn">The type of the t return.</typeparam>
        /// <param name="eventType">Type of the event.</param>
        /// <param name="handler">The handler.</param>
        static public void RemoveListener<TReturn>(string eventType, Func<T, U, TReturn> handler)
        {
            MessengerInternal.RemoveListener(eventType, handler);
        }

        /// <summary>
        /// Broadcasts the specified event type.
        /// </summary>
        /// <param name="eventType">Type of the event.</param>
        /// <param name="arg1">The arg1.</param>
        /// <param name="arg2">The arg2.</param>
        static public void Broadcast(string eventType, T arg1, U arg2)
        {
            Broadcast(eventType, arg1, arg2, MessengerInternal.DEFAULT_MODE);
        }

        /// <summary>
        /// Broadcasts the specified event type.
        /// </summary>
        /// <typeparam name="TReturn">The type of the t return.</typeparam>
        /// <param name="eventType">Type of the event.</param>
        /// <param name="arg1">The arg1.</param>
        /// <param name="arg2">The arg2.</param>
        /// <param name="returnCall">The return call.</param>
        static public void Broadcast<TReturn>(string eventType, T arg1, U arg2, Action<TReturn> returnCall)
        {
            Broadcast(eventType, arg1, arg2, returnCall, MessengerInternal.DEFAULT_MODE);
        }

        /// <summary>
        /// Broadcasts the specified event type.
        /// </summary>
        /// <param name="eventType">Type of the event.</param>
        /// <param name="arg1">The arg1.</param>
        /// <param name="arg2">The arg2.</param>
        /// <param name="mode">The mode.</param>
        static public void Broadcast(string eventType, T arg1, U arg2, MessengerMode mode)
        {
            MessengerInternal.OnBroadcasting(eventType, mode);
            var invocationList = MessengerInternal.GetInvocationList<Action<T, U>>(eventType);

            foreach (var callback in invocationList)
                callback.Invoke(arg1, arg2);
        }

        /// <summary>
        /// Broadcasts the specified event type.
        /// </summary>
        /// <typeparam name="TReturn">The type of the t return.</typeparam>
        /// <param name="eventType">Type of the event.</param>
        /// <param name="arg1">The arg1.</param>
        /// <param name="arg2">The arg2.</param>
        /// <param name="returnCall">The return call.</param>
        /// <param name="mode">The mode.</param>
        static public void Broadcast<TReturn>(string eventType, T arg1, U arg2, Action<TReturn> returnCall, MessengerMode mode)
        {
            MessengerInternal.OnBroadcasting(eventType, mode);
            var invocationList = MessengerInternal.GetInvocationList<Func<T, U, TReturn>>(eventType);

            foreach (var result in invocationList.Select(del => del.Invoke(arg1, arg2)).Cast<TReturn>())
            {
                returnCall.Invoke(result);
            }
        }
    }


    // Three parameters
    /// <summary>
    /// Class Messenger with 3 parameters.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="U"></typeparam>
    /// <typeparam name="V"></typeparam>
    static public class Messenger<T, U, V>
    {
        /// <summary>
        /// Adds the listener.
        /// </summary>
        /// <param name="eventType">Type of the event.</param>
        /// <param name="handler">The handler.</param>
        static public void AddListener(string eventType, Action<T, U, V> handler)
        {
            MessengerInternal.AddListener(eventType, handler);
        }

        /// <summary>
        /// Adds the listener.
        /// </summary>
        /// <typeparam name="TReturn">The type of the t return.</typeparam>
        /// <param name="eventType">Type of the event.</param>
        /// <param name="handler">The handler.</param>
        static public void AddListener<TReturn>(string eventType, Func<T, U, V, TReturn> handler)
        {
            MessengerInternal.AddListener(eventType, handler);
        }

        /// <summary>
        /// Removes the listener.
        /// </summary>
        /// <param name="eventType">Type of the event.</param>
        /// <param name="handler">The handler.</param>
        static public void RemoveListener(string eventType, Action<T, U, V> handler)
        {
            MessengerInternal.RemoveListener(eventType, handler);
        }

        /// <summary>
        /// Removes the listener.
        /// </summary>
        /// <typeparam name="TReturn">The type of the t return.</typeparam>
        /// <param name="eventType">Type of the event.</param>
        /// <param name="handler">The handler.</param>
        static public void RemoveListener<TReturn>(string eventType, Func<T, U, V, TReturn> handler)
        {
            MessengerInternal.RemoveListener(eventType, handler);
        }

        /// <summary>
        /// Broadcasts the specified event type.
        /// </summary>
        /// <param name="eventType">Type of the event.</param>
        /// <param name="arg1">The arg1.</param>
        /// <param name="arg2">The arg2.</param>
        /// <param name="arg3">The arg3.</param>
        static public void Broadcast(string eventType, T arg1, U arg2, V arg3)
        {
            Broadcast(eventType, arg1, arg2, arg3, MessengerInternal.DEFAULT_MODE);
        }

        /// <summary>
        /// Broadcasts the specified event type.
        /// </summary>
        /// <typeparam name="TReturn">The type of the t return.</typeparam>
        /// <param name="eventType">Type of the event.</param>
        /// <param name="arg1">The arg1.</param>
        /// <param name="arg2">The arg2.</param>
        /// <param name="arg3">The arg3.</param>
        /// <param name="returnCall">The return call.</param>
        static public void Broadcast<TReturn>(string eventType, T arg1, U arg2, V arg3, Action<TReturn> returnCall)
        {
            Broadcast(eventType, arg1, arg2, arg3, returnCall, MessengerInternal.DEFAULT_MODE);
        }

        /// <summary>
        /// Broadcasts the specified event type.
        /// </summary>
        /// <param name="eventType">Type of the event.</param>
        /// <param name="arg1">The arg1.</param>
        /// <param name="arg2">The arg2.</param>
        /// <param name="arg3">The arg3.</param>
        /// <param name="mode">The mode.</param>
        static public void Broadcast(string eventType, T arg1, U arg2, V arg3, MessengerMode mode)
        {
            MessengerInternal.OnBroadcasting(eventType, mode);
            var invocationList = MessengerInternal.GetInvocationList<Action<T, U, V>>(eventType);

            foreach (var callback in invocationList)
                callback.Invoke(arg1, arg2, arg3);
        }

        /// <summary>
        /// Broadcasts the specified event type.
        /// </summary>
        /// <typeparam name="TReturn">The type of the t return.</typeparam>
        /// <param name="eventType">Type of the event.</param>
        /// <param name="arg1">The arg1.</param>
        /// <param name="arg2">The arg2.</param>
        /// <param name="arg3">The arg3.</param>
        /// <param name="returnCall">The return call.</param>
        /// <param name="mode">The mode.</param>
        static public void Broadcast<TReturn>(string eventType, T arg1, U arg2, V arg3, Action<TReturn> returnCall, MessengerMode mode)
        {
            MessengerInternal.OnBroadcasting(eventType, mode);
            var invocationList = MessengerInternal.GetInvocationList<Func<T, U, V, TReturn>>(eventType);

            foreach (var result in invocationList.Select(del => del.Invoke(arg1, arg2, arg3)).Cast<TReturn>())
            {
                returnCall.Invoke(result);
            }
        }
    }


}
