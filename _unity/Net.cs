﻿using UnityEngine;
using System.Collections;
using System;
using System.Net.Sockets;

namespace _unity.Networking
{
    class Net
    {
        IEnumerator PingAddress(string address, float timeOut, Action<bool> Callback)
        {
            WWW www = new WWW(address);

            float startTime = Time.time;

            float timer = 0;
            bool failed = false;

            while (!www.isDone)
            {
                if (timer > timeOut) { failed = true; break; }
                timer += Time.deltaTime;
                yield return null;
            }
            if (failed || !string.IsNullOrEmpty(www.error))
                Callback(false);
            else
                Callback(true);

        }

        bool CheckSocketOpen(string address, int port, float timeOut = 2f)
        {
            using (var socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp))
            {
                IAsyncResult result = socket.BeginConnect(address, port, null, null);

                bool success = result.AsyncWaitHandle.WaitOne((int)(timeOut * 1000), true);
                socket.Close();

                return success;
            }
        }

        public IEnumerator CheckIPWithPort(string ip, int port, float timeout, Action<bool> Callback)
        {
            yield return PingAddress(ip, timeout, (bool done) =>
            {
                Callback(done && CheckSocketOpen(ip, port, timeout));
            });
        }
    }
}
